// @flow

import { Logger } from 'aws-amplify'
import moment from 'moment-timezone'
import uniq from 'lodash/uniq'
import type { State, Actions, SetTzSearchAction, AddTimezoneAction, RemoveTimezoneAction, SetHomeTimezoneAction, SetSelectedHourAction, SetSelectedDateAction } from './types'

const logger = new Logger('reducer', 'INFO')
const tzGuess = moment.tz.guess()
const defaultState = {
  timezones: [tzGuess],
  homeTimezone: tzGuess,
  selectedHour: null,
  selectedDate: moment().toISOString(),
  tzSearchString: '',
}

const actions = {
  'ADD_TIMEZONE': (state: State, action: AddTimezoneAction) => {
    return { ...state, timezones: uniq([ ...state.timezones, action.timezone ]) }
  },
  'REMOVE_TIMEZONE': (state: State, action: RemoveTimezoneAction) => {
    return { ...state, timezones: state.timezones.filter(tz => tz !== action.timezone) }
  },
  'SET_HOME_TIMEZONE': (state: State, action: SetHomeTimezoneAction) => {
    return { ...state, homeTimezone: action.timezone }
  },
  'SET_SELECTED_HOUR': (state: State, action: SetSelectedHourAction) => {
    return { ...state, selectedHour: action.hour }
  },
  'SET_SELECTED_DATE': (state: State, action: SetSelectedDateAction) => {
    return { ...state, selectedDate: action.date }
  },
  'SET_TZ_SEARCH': (state: State, action: SetTzSearchAction) => {
    return { ...state, tzSearchString: action.searchString }
  },
}

const reducer = function(state: State = defaultState, action: Actions): State {
  logger.debug('action', action)
  switch (action.type) {
    case 'ADD_TIMEZONE':
      return actions[action.type](state, action)
    case 'REMOVE_TIMEZONE':
      return actions[action.type](state, action)
    case 'SET_HOME_TIMEZONE':
      return actions[action.type](state, action)
    case 'SET_SELECTED_HOUR':
      return actions[action.type](state, action)
    case 'SET_SELECTED_DATE':
      return actions[action.type](state, action)
    case 'SET_TZ_SEARCH':
      return actions[action.type](state, action)
    default:
      return state
  }
}

export default reducer


