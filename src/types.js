// @flow

export type Timezone = string
export type Timezones = Array<Timezone>
export type SelectedHour = ?string
export type SelectedDate = string

export type State = {
  +timezones: Timezones,
  +homeTimezone: Timezone,
  +selectedHour: SelectedHour,
  +selectedDate: SelectedDate,
  +tzSearchString: string,
}

export type AddTimezoneAction = { type: 'ADD_TIMEZONE', timezone: Timezone }
export type RemoveTimezoneAction = { type: 'REMOVE_TIMEZONE', timezone: Timezone }
export type SetHomeTimezoneAction = { type: 'SET_HOME_TIMEZONE', timezone: Timezone }
export type SetSelectedHourAction = { type: 'SET_SELECTED_HOUR', hour: SelectedHour }
export type SetSelectedDateAction = { type: 'SET_SELECTED_DATE', date: SelectedDate }
export type SetTzSearchAction = { type: 'SET_TZ_SEARCH', searchString: string }

export type Actions = AddTimezoneAction | RemoveTimezoneAction | SetHomeTimezoneAction | SetSelectedHourAction | SetSelectedDateAction | SetTzSearchAction
