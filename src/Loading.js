// @flow

import React from 'react'

type Props = {
  error: boolean,
  timedOut: boolean,
  pastDelay: boolean
}

export default function(props: Props) {
  if (props.error) {
    return <div>Error!</div>
  } else if (props.timedOut) {
    return <div>Taking a long time...</div>
  } else if (props.pastDelay) {
    return <div>Loading...</div>
  } 
  return null
}

