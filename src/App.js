// @flow

import React from 'react'
import { Route } from 'react-router-dom'
import MyLayout from './components/MyLayout'
//import Loadable from 'react-loadable'
//import Loading from './Loading'

//const LoadableAbout = Loadable({
//  loader: () => import('./components/About'),
//  loading: Loading
//})

function App() {
  return (
    <div className="App">
      <Route exact path="/" component={MyLayout} />
      {/*<Route exact path="/about" component={LoadableAbout} />*/}
    </div>
  )
}

export default App
