// @flow
/**
 * Modified from https://github.com/kevinroberts/city-timezones
 */

import filter from 'lodash/filter'
import includes from 'lodash/includes'
import cityMapping from 'city-timezones/data/cityMap.json'
cityMapping.push({ city: 'Etc/UTC', timezone: 'UTC'})
cityMapping.push({ city: 'Swatch Internet Time', timezone: 'SwatchInternetTime'})

export const lookupViaCity = (city: string) => {
  const cityLookup = filter(cityMapping, o => includes(o.city.toLowerCase(), city.toLowerCase()))
  if (cityLookup && cityLookup.length > 0) {
    return cityLookup
  } else {
    return []
  }
}

