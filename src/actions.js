// @flow

import type { SetTzSearchAction, SetSelectedDateAction, SelectedDate, SetSelectedHourAction, SelectedHour, SetHomeTimezoneAction, RemoveTimezoneAction, AddTimezoneAction, Timezone } from './types'

export function addTimezone(timezone: Timezone): AddTimezoneAction {
  return {
    type: 'ADD_TIMEZONE',
    timezone
  }
}
export function removeTimezone(timezone: Timezone): RemoveTimezoneAction {
  return {
    type: 'REMOVE_TIMEZONE',
    timezone
  }
}
export function setHomeTimezone(timezone: Timezone): SetHomeTimezoneAction {
  return {
    type: 'SET_HOME_TIMEZONE',
    timezone
  }
}
export function setSelectedHour(hour: SelectedHour): SetSelectedHourAction {
  return {
    type: 'SET_SELECTED_HOUR',
    hour
  }
}
export function setSelectedDate(date: SelectedDate): SetSelectedDateAction {
  return {
    type: 'SET_SELECTED_DATE',
    date
  }
}
export function setTzSearch(searchString: string): SetTzSearchAction {
  return {
    type: 'SET_TZ_SEARCH',
    searchString
  }
}

