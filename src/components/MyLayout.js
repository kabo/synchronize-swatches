// @flow

import React, { Component } from 'react'
import {connect} from 'react-redux'
import { Logger } from 'aws-amplify'
import AntLayout from 'antd/lib/layout'
import IntervalRenderer from 'react-interval-renderer'
import MainContent from './MainContent'

const logger = new Logger('MyLayout', 'INFO') // eslint-disable-line no-unused-vars
const { Header, Content, Footer } = AntLayout

type Props = { }

class MyLayout extends Component<Props> {
  render() {
    return (
      <div className="Layout">
        <AntLayout>
          <Header className="site-header">
            <h1 className="header">
              Synchronize Swatches!
            </h1>
          </Header>
          <AntLayout>
            <Content style={{padding: '0 20px'}}>
              <IntervalRenderer interval={60000}>
                <MainContent />
              </IntervalRenderer>
            </Content>
          </AntLayout>
          <Footer className="site-footer">
            <p><a href="https://gitlab.com/kabo/synchronize-swatches">Source on GitLab</a>, released under <a href="http://coil.apotheon.org/">Copyfree Open Innovation License 0.5</a></p>
            <p>favicon: <a href="https://thenounproject.com/term/smartwatch-sync/628536/">smartwatch sync</a> by Aybige from the Noun Project</p>
          </Footer>
        </AntLayout>
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
  }
}

export default connect(mapStateToProps)(MyLayout)

