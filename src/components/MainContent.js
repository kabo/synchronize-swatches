// @flow

import React, { Component } from 'react'
import {connect} from 'react-redux'
import { Logger } from 'aws-amplify'
import Table from 'antd/lib/table'
import Icon from 'antd/lib/icon'
import Button from 'antd/lib/button'
import Tooltip from 'antd/lib/tooltip'
import DatePicker from 'antd/lib/date-picker'
import moment from 'moment-timezone'
import RSelect from 'react-select'
import padStart from 'lodash/padStart'
import { lookupViaCity } from '../tzSearcher'
import { setTzSearch, addTimezone, removeTimezone, setHomeTimezone, setSelectedHour, setSelectedDate } from '../actions'
import type { SetTzSearchAction, Actions, AddTimezoneAction, RemoveTimezoneAction, SetHomeTimezoneAction, Timezones, Timezone, SetSelectedHourAction, SelectedHour, SetSelectedDateAction, SelectedDate } from '../types'

const logger = new Logger('MainContent', 'INFO') // eslint-disable-line no-unused-vars

const SITTZ = 'SwatchInternetTime'
moment.tz.add(`${SITTZ}|beat|-10|0|`)
const renderBeat = ({time, withAt}: {time?: moment, withAt?: boolean}): string => {
  if (!time || !moment.isMoment(time)) {
    time = moment.utc()
  }
  time = time.clone().utcOffset(1)
  const hours   = time.hour()
  const minutes = time.minute()
  const seconds = time.second()
  const numbers = padStart(Math.floor((seconds + minutes*60 + hours*3600) / 86.4), 3, '0')
  const prefix = withAt ? '@' : ''
  return prefix+numbers
}
const tzSorter = (t1, t2) => {
  const a = parseInt(t1.moment.format('Z').replace(':', ''), 10)
  const b = parseInt(t2.moment.format('Z').replace(':', ''), 10)
  if (a === b) return 0
  return a > b ? -1 : 1
}

type Props = {
  timezones: Timezones,
  homeTimezone: Timezone,
  selectedHour: SelectedHour,
  selectedDate: SelectedDate,
  tzSearchString: string,
  addTimezone: (timezone: mixed) => AddTimezoneAction,
  removeTimezone: (timezone: Timezone) => RemoveTimezoneAction,
  setHomeTimezone: (timezone: Timezone) => SetHomeTimezoneAction,
  setSelectedHour: (hour: SelectedHour) => SetSelectedHourAction,
  setSelectedDate: (date: SelectedDate) => SetSelectedDateAction,
  setTzSearch: (searchString: string) => SetTzSearchAction,
}

class MainContent extends Component<Props> {
  render() {
    const { setTzSearch, tzSearchString, setSelectedDate, selectedDate, setSelectedHour, selectedHour, timezones, homeTimezone, addTimezone, removeTimezone, setHomeTimezone } = this.props
    const addableTimezones = tzSearchString ? lookupViaCity(tzSearchString).slice(0, 10).map(tz => ({value: tz.timezone, label: `${tz.city} (${tz.timezone})`})) : []
    const homeDate = moment.tz(selectedDate, homeTimezone)
    const now = homeDate.clone().utc()
    //if (now.get('minute') > 29) {
    //  now.add(1, 'hour')
    //}
    now.startOf('hour')
    const nowHour = now.format('HH')
    const tableTimezones = timezones.map(tz => ({
      tz,
      key: tz,
      moment: moment.tz(now, tz),
    }))
      .sort(tzSorter)
    const homeNow = moment.tz(now, homeTimezone)
    const homeNowOffset = homeNow.utcOffset()
    const columns = [
      {
        key: 'tz-actions',
        render: (text, time) => {
          const star = time.tz === homeTimezone ? <Icon type="star" /> : <Icon type="star-o" style={{cursor: 'pointer'}} onClick={() => setHomeTimezone(time.tz)} />
          return (<div className="tz-actions">
            <Icon type="delete" style={{cursor: 'pointer'}} onClick={() => removeTimezone(time.tz)} />
            {star}
          </div>)
        }
      },
      {
        key: 'tz-diff',
        render: (text, time) => {
          let diffText = ''
          if (time.tz !== homeTimezone) {
            const diffToHomeTime = moment.duration(time.moment.utcOffset()-homeNowOffset, 'minutes')
            let hourDiff = diffToHomeTime.hours()
            const hourDiffText = hourDiff < 0 ? '-'+(''+Math.abs(hourDiff)).padStart(2, '0') : '+'+(''+hourDiff).padStart(2, '0')
            diffText = `${hourDiffText}:${(''+Math.abs(diffToHomeTime.minutes())).padStart(2, '0')}`
          }
          return <div className="tz-diff">{diffText}</div>
        }
      },
      {
        key: 'tz-info',
        render: (text, time) => {
          return (<div className="tz-info">
            <span className="tz-name">{time.tz}</span>
            <Tooltip title={`UTC ${time.moment.format('Z')}`}>
              <span className="tz-abbr">{time.moment.zoneAbbr()} </span>
            </Tooltip>
          </div>)
        }
      },
      {
        key: 'tz-time',
        render: (text, time) => <div className="tz-time">
          {time.tz === SITTZ ? renderBeat({withAt: true}) : moment.tz(time.tz).format('HH:mm')}
        </div>
      },
    ]
    const startOfHomeDayInUTC = homeDate.clone().startOf('day').utc()
    for (let i=0; i<24; ++i) {
      if (i > 0) {
        startOfHomeDayInUTC.add(1, 'hour')
      }
      const utcTime = startOfHomeDayInUTC.format()
      const utcHour = startOfHomeDayInUTC.format('HH')
      columns.push({
        key: `tz-${i}`,
        render: (text, time) => {
          const localCurTime = moment.tz(utcTime, time.tz)
          const localCurHour = localCurTime.format('HH')
          const localCurMinutes = localCurTime.format('mm')
          let className = 'tz-day tz-timeline'
          if ((localCurHour < 6 || localCurHour > 21) && time.tz !== SITTZ) {
            className = 'tz-night tz-timeline'
          }
          if (localCurHour === '00' && time.tz !== SITTZ) {
            className += ' tz-start'
          }
          if (localCurHour === '23' && time.tz !== SITTZ) {
            className += ' tz-end'
          }
          if (selectedHour === null && utcHour === nowHour) {
            className += ' tz-timeline-selected'
          } else if (utcHour === selectedHour) {
            className += ' tz-timeline-selected'
          }
          return (<div className={className} onMouseOver={() => setSelectedHour(utcHour)}>
            <div className="middle">
              <div className="tz-timeline-hour">{time.tz === SITTZ ? renderBeat({time: localCurTime, withAt: false}) : localCurHour}</div>
              {localCurMinutes !== '00' && <div className="tz-timeline-minutes">{localCurMinutes}</div>}
            </div>
          </div>)
        }
      })
    }
    return (
      <div className="main-content">
        <div className="controls">
          <RSelect
            placeholder="Add timezone"
            onChange={addTimezone}
            onInputChange={setTzSearch}
            filterOption={() => true}
            inputValue={tzSearchString}
            options={addableTimezones}
            escapeClearsValue={true}
            value={null}
            noOptionsMessage={({inputValue}) => inputValue === '' ? 'Type to search' : 'Nothing found'}
            className="select-timezone"
          />
          <Button icon="arrow-left" onClick={() => setSelectedDate(homeDate.clone().subtract(1, 'day').toISOString())} />
          <DatePicker
            value={homeDate}
            allowClear={false}
            onChange={(m, date) => setSelectedDate(date)}
          />
          <Button icon="arrow-right" onClick={() => setSelectedDate(homeDate.clone().add(1, 'day').toISOString())} />
        </div>
        <div onMouseOut={() => setSelectedHour(null)}>
          <Table
            className="timezone-table"
            columns={columns}
            dataSource={tableTimezones}
            pagination={false}
          />
        </div>
        <div className="push"></div>
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
    timezones: state.timezones,
    homeTimezone: state.homeTimezone,
    selectedHour: state.selectedHour,
    selectedDate: state.selectedDate,
    tzSearchString: state.tzSearchString,
  }
}

const mapDispatchToProps = function(dispatch: (Actions) => {}) {
  return {
    addTimezone: (timezone) => dispatch(addTimezone(timezone.value)),
    removeTimezone: (timezone) => dispatch(removeTimezone(timezone)),
    setHomeTimezone: (timezone) => dispatch(setHomeTimezone(timezone)),
    setSelectedHour: (hour) => dispatch(setSelectedHour(hour)),
    setSelectedDate: (date) => dispatch(setSelectedDate(date)),
    setTzSearch: (searchString) => dispatch(setTzSearch(searchString)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainContent)


