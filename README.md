# Synchronize Swatches!

A simple web interface that lets you add timezones and easily see what the time is and will be in different timezones.

## Run it locally

```shell
git clone https://gitlab.com/kabo/synchronize-swatches.git
cd synchronize-swatches
yarn install
yarn start
```

## Why is it called "Synchronize Swatches!"?

If you watched Parker Lewis Can't Lose you know the importance of synchronizing your watches before putting any plan into effect. This tool helps people across timezones do what they need to do at the time they need to do it.

## Changelog

### 0.2.2 (2018-06-02)

- Swatch Internet Time bugfix

### 0.2.1 (2018-06-02)

- Added [Swatch Internet Time](https://en.wikipedia.org/wiki/Swatch_Internet_Time)

### 0.2.0 (2018-06-02)

- Can search for cities within timezones (#4)

### 0.1.1 (2018-05-12)

- Rerenders every minute to update the current time and move the timeline
- Styling, minor fixes

### 0.1.0 (2018-05-11)

- First release

## License

Released under [COIL 0.5](./LICENSE.md) ([Copyfree Open Innovation License](http://coil.apotheon.org/)).

Project icon: [smartwatch sync](https://thenounproject.com/term/smartwatch-sync/628536/) by Aybige from the Noun Project

